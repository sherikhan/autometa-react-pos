﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ReactDotNetCoreApp.Models;

namespace ReactDotNetCoreApp.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        [HttpPost("[action]")]
        public IActionResult RegisterUser([FromBody]User userObj)
        {
            #region Register
            if (Constants.ACCESS_TOKEN == userObj.AccessToken)
            {
                if (Authenticate(userObj))
                {
                    var backUser = new User();
                    backUser.UserName = userObj.UserName;
                    backUser.LoginName = userObj.LoginName;
                    backUser.Password = userObj.Password;
                    backUser.RoleId = Convert.ToInt32(userObj.strRoleId);
                    backUser.BusinessUnitId = Convert.ToInt32(userObj.strBusinessUnitId);
                    backUser.AddUser(backUser);
                    if (backUser.UserId > 0)
                        return Json(new { error = "false", message = "Success! User Created" });
                    else
                        return Json(new { error = "true", message = "Failed! Error occured at Database End" });

                }
                else
                    return Json(new { error = "true", message = "Failed! Enter all required fields" });
            }
            #endregion

            return Json(new { error = "true", message = "Error! Provided AccessToken is not correct" });
        }

        [HttpPost("[action]")]
        public IActionResult Login([FromBody]User userObj)
        {
           
            #region Login
            if (Constants.ACCESS_TOKEN == userObj.AccessToken)
            {
                if (!string.IsNullOrEmpty(userObj.LoginName) && !string.IsNullOrEmpty(userObj.Password))
                {
                    var backUser = new User();
                    backUser.LoginName = userObj.LoginName;
                    backUser.Password = userObj.Password;
                    var user = backUser.Authenticate(backUser);
                    if (user != null)
                    {
                        return Json(new { error = "false", message = "Success!", user });
                    }
                    else
                        return Json(new { error = "true", message = "Failed! Error occured at Database End" });

                }
                else
                    return Json(new { error = "true", message = "Failed! Enter all required fields" });
            }
            
            return Json(new { error = "true", message = "Error! Provided AccessToken is not correct" });
            #endregion
        }

        //[HttpGet("[action]")]
        //public JsonResult GetAllUsers()
        //{
        //    var ls = new User().GetAllUsers();
        //    var newlss = ls.Select(x => new
        //    {
        //        x.UserId,
        //        x.UserName,
        //        x.LoginName,
        //        x.strRoleId,
        //    }).ToList();

        //    return Json(newlss);
        //}
        [HttpGet("[action]")]
        public IEnumerable<User> GetAllUsers()
        {
            var ls = new User().GetAllUsers();
            return ls.Select(index => new User
            {
                UserId = index.UserId,
                UserName = index.UserName,
                LoginName = index.LoginName,
                strRoleId = index.strRoleId,

            });
        }
        private bool Authenticate(User userObj)
        {
            if (string.IsNullOrEmpty(userObj.strBusinessUnitId) || string.IsNullOrEmpty(userObj.LoginName)
                || string.IsNullOrEmpty(userObj.Password) || string.IsNullOrEmpty(userObj.strRoleId))
            {
                return false;
            }
            return true;
        }
    }
}