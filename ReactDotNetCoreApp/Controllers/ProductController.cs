﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ReactDotNetCoreApp.Models;

namespace ReactDotNetCoreApp.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        #region Form Submition Methods

        [HttpPost("[action]")]
        public IActionResult AddProduct([FromBody]Product productObj)
        {
            

            if (productObj != null && !string.IsNullOrEmpty(productObj.BarCode) && !string.IsNullOrEmpty(productObj.ProductName))
            {
                if (productObj.ProductId > 0)
                {
                    productObj.UpdatedOn = DateTime.Now;
                    productObj.UpdatedBy = productObj.CreatedBy;
                    productObj.Value = productObj.SaleRate * productObj.OpeningQty;

                    var res = new Product().UpdateProduct(productObj);
                    if (res > 0)
                    {
                        return Json(new { error = "false", message = "Product Updated Successfully!", res });
                    }
                    else
                        return Json(new { error = "true", message = "Failed! Error occured while saving Product." });
                }
                else
                {
                    #region Add New Product
                    productObj.CreatedOn = DateTime.Now;
                    productObj.Value = productObj.SaleRate * productObj.OpeningQty;

                    var res = new Product().AddProduct(productObj);
                    if (res > 0)
                    {
                        return Json(new { error = "false", message = "Product Added Successfully!", res });
                    }
                    else
                        return Json(new { error = "true", message = "Failed! Error occured while saving Product." });
                    #endregion
                }

            }
            else
                return Json(new { error = "true", message = "Failed! Enter all required fields" });

            
        }
        #endregion

        #region Remote Functions

        [HttpGet("[action]")]
        public IEnumerable<Product> GetAllProducts()
        {
            var ls = new Product().GetAllProducts().Take(100);
            return ls;
        }

        [HttpGet("[action]")]
        public IEnumerable<Category> GetAllCategories()
        {
            var ls = new Category().GetCategoriesList();
            return ls.Select(index => new Category
            {
                CategoryId = index.CategoryId,
                CategoryName = index.CategoryName,
            });
        }

        [HttpGet("[action]")]
        public Product GetProductDetails(int? id)
        {
            if (Convert.ToInt32(id) > 0) //!string.IsNullOrEmpty(id) &&
            {
                var res = new Product().GetProductDetails(Convert.ToInt32(id));
                return res;
            }
            return null;
        }

        [HttpDelete("[action]")]
        public IActionResult DeleteProduct(int id)
        {
            if (Convert.ToInt32(id) > 0)
            {
                var res = new Product().DeleteProduct(id);
                return Json(new { error = "false", message = "Product Deleted Scuccessfully!" });
            }
            return Json(new { error = "true", message = "Failed! An Error Occured" });
        }

        [HttpPost("[action]")]
        public IActionResult DisableProduct(int id)
        {
            if (Convert.ToInt32(id) > 0)
            {
                var res = new Product().InactiveProduct(id);
                return Json(new { error = "false", message = "Product Status Changed Scuccessfully!" });
            }
            return Json(new { error = "true", message = "Failed! An Error Occured" });

        }

        #endregion
    }
}