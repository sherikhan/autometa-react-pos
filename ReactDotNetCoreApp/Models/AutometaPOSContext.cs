﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactDotNetCoreApp.Models
{
    public class AutometaPOSContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer(@"Server=(local)\DANISH;Database=AutometaPOS;Integrated Security=SSPI;");
                optionsBuilder.UseSqlServer(@"Server=DESKTOP-FP10FF9\MSSQLSERVER2017;Database=AutometaPOS;Integrated Security=SSPI;");
            }
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<User>()
        //        .HasOne<Role>(s => s.Role)
        //        .WithOne(g => g.User)
        //        .HasForeignKey<Role>(s => s.RoleId);
        //}

        public DbSet<User> User { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<BusinessUnit> BusinessUnit { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<Category> Category { get; set; }
    }
}
