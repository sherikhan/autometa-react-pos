﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReactDotNetCoreApp.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string BarCode { get; set; }
        [Required]
        public int BusinessUnitId { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(200)")]
        public string ProductName { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string Description { get; set; }
        
        //[Column(TypeName = "nvarchar(500)")]
        //public string productSpecification { get; set; }

        [Column(TypeName = "nvarchar(10)")]
        public string Unit { get; set; }
        public int OpeningQty { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? SaleRate { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? PurchaseRate { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? Value { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Weight { get; set; }
        public int? MaximumQty { get; set; }
        public int? MinimumQty { get; set; }
        public string Location { get; set; }
        public bool Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public Category Category { get; set; }

        public int AddProduct(Product obj)
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    context.Product.Add(obj);
                    context.SaveChanges();
                    return obj.ProductId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int UpdateProduct(Product obj)
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    //context.Product.Add(obj);
                    context.Entry(obj).State = EntityState.Modified;
                    context.SaveChanges();
                    return obj.ProductId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public Product GetProductDetails(int Id)
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    var  obj = context.Product.Where(x => x.ProductId == Id).FirstOrDefault();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InactiveProduct(int Id)
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    var obj = context.Product.Where(x => x.ProductId == Id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Status = obj.Status ? false :  true ;
                        context.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteProduct(int Id)
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    var obj = context.Product.Where(x => x.ProductId == Id).FirstOrDefault();
                    context.Remove(obj);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<Product> GetAllProducts()
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    var list = context.Product.ToList();
                    var categories = context.Category.ToList();
                    return list;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IEnumerable<Product> GetAllActiveProducts()
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    var list = context.Product.Where(x => x.Status == true).ToList();
                    var categories = context.Category.ToList();
                    return list;
                    //var list = context.User.ToList();
                    //var roles = context.Role.ToList();

                    //foreach (var item in list)
                    //{
                    //    //item.Role = roles.Where(x => x.RoleId == item.RoleId).FirstOrDefault();
                    //    item.strRoleId = roles.Where(x => x.RoleId == item.RoleId).FirstOrDefault().RoleTitle;
                    //}
                    //return list;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool Delete(int Id)
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    var obj = context.Product.Where(x => x.ProductId == Id).FirstOrDefault();
                    context.Remove(obj);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
