﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReactDotNetCoreApp.Models
{

    public class User
    {
        [Key]
        public int UserId { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(20)")]
        public string LoginName { get; set; }
        [Column(TypeName = "nvarchar(200)")]
        public string UserName { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(200)")]
        public string Password { get; set; }
        [Required]
        public int RoleId { get; set; }
        [Required]
        public int BusinessUnitId { get; set; }

        public Role Role { get; set; }
        public BusinessUnit BusinessUnit { get; set; }

        [NotMapped]
        public string strRoleId { get; set; }
        [NotMapped]
        public string strBusinessUnitId { get; set; }
        [NotMapped]
        public string AccessToken { get; set; }


        public long AddUser(User obj)
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    context.User.Add(obj);
                    context.SaveChanges();
                    return obj.UserId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public User Authenticate(User obj)
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    var userObj = context.User.Where(x => x.LoginName == obj.LoginName && x.Password == obj.Password).FirstOrDefault();
                    if (userObj != null)
                    {
                        userObj.strRoleId = userObj.RoleId.ToString();
                        userObj.strBusinessUnitId = userObj.BusinessUnitId.ToString();
                        //userObj.log
                        //context.SaveChanges();
                        return userObj;
                    }
                    
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<User> GetAllUsers()
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    var list = context.User.ToList();
                    var roles = context.Role.ToList();

                    foreach (var item in list)
                    {
                        //item.Role = roles.Where(x => x.RoleId == item.RoleId).FirstOrDefault();
                        item.strRoleId = roles.Where(x => x.RoleId == item.RoleId).FirstOrDefault().RoleTitle;
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        //User For Drop Down
        //List<SelectListItem> item = YearList.ConvertAll(a =>
        //{
        //    return new SelectListItem()
        //    {
        //        Text = a.ToString(),
        //        Value = a.ToString(),
        //        Selected = false
        //    };
        //});

    }
}
