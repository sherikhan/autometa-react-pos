﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReactDotNetCoreApp.Models
{
    public class BusinessUnit
    {
        [Key]
        public int BusinessUnitId { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(200)")]
        public string Name { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(10)")]
        public string ShortName { get; set; }
        //public ICollection<User> User { get; set; }
    }
}
