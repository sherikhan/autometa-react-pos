﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReactDotNetCoreApp.Models
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string CategoryName { get; set; }
        public bool Status { get; set; }

        public IEnumerable<Category> GetCategoriesList()
        {
            try
            {
                using (var context = new AutometaPOSContext())
                {
                    var list = context.Category.Where(x => x.Status == true).ToList();
                    return list;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
