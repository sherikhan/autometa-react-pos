﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReactDotNetCoreApp.Models
{
    public class Role
    {
        [Key]
        public int RoleId { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string RoleTitle { get; set; }
        public User User { get; set; }
    }
}
