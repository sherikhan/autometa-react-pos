﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon} from 'react-bootstrap';

export class ProductList extends Component {
    displayName = ProductList.name

    constructor(props) { 
        super(props);
        this.state = { products: [], loading: true, message: '' };

        this.loadProducts();

        this.handelDelete = this.handelDelete.bind(this);
    }


    handelDelete(pram) {
        //debugger;
        this.requestDelete(pram);
        //console.log(pram);
    }

    requestDelete(pram) {
        //debugger
        fetch(`api/Product/DeleteProduct/?id=${pram}`, {
            method: "DELETE",
            cache: "no-cache", // *default, no-cache, reload,
            //body: JSON.stringify(pram),
            headers: {
                "Content-type": "application/json"
            }
        }).then(res => res.json())
            .then(response => {
                if (response.error === 'false') {
                    //empty input fields
                    this.loadProducts();
                }
                this.setState({ message: response.message });               
            })
            .catch(error => console.log(error));
    }

    loadProducts() {
        fetch('api/Product/GetAllProducts')
            .then(response => response.json())
            .then(data => {
                //console.log(data);
                this.setState({ products: data, loading: false });
            });
    }

    handelInactive(pram) {
        //debugger;
        fetch(`api/Product/DisableProduct/?id=${pram}`, {
            method: "POST",
            cache: "no-cache", // *default, no-cache, reload,
            headers: {
                "Content-type": "application/json"
            }
        }).then(res => res.json())
            .then(response => {
                if (response.error === 'false') {
                    //empty input fields
                    this.loadProducts();
                }
                this.setState({ message: response.message });
            })
            .catch(error => console.log(error));


    }

    renderUsersTable(products) {
        return (
            <div>
                <span className="label label-warning">{this.state.message}</span>

                <table className='table'>
                    <thead>
                        <tr>
                            <th>Bar Code</th>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Opening Qty</th>
                            <th>Purchase Rate</th>
                            <th>Sale Rate</th>
                            <th>Value</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {products.map(product =>
                            <tr key={product.productId}>
                                <td>{product.barCode}</td>
                                <td>{product.productName}</td>
                                <td>{product.category.categoryName}</td>
                                <td>{product.openingQty}</td>
                                <td>{product.purchaseRate}</td>
                                <td>{product.saleRate}</td>
                                <td>{product.value}</td>
                                <td>{ product.status ? 'Active' : 'Inactive' }</td>
                                <td>
                                    <Link title='Edit' to={'/ProductCreation/' + product.productId}><Glyphicon glyph='edit' /></Link>

                                    &nbsp;
                                   <button className='btn btn-xs' title='Enable/Disable'
                                        onClick={() => { if (window.confirm('Do you want to change status of Product?')) { this.handelInactive(product.productId) } }} >
                                        <Glyphicon glyph='eye-open' />
                                    </button>
                                    

                                    &nbsp;
                                <button className='btn btn-xs btn-danger'
                                        onClick={() => { if (window.confirm('Delete Product?')) { this.handelDelete(product.productId) } }} >
                                        <Glyphicon glyph='trash' />
                                    </button>
                                    



                                    {/*
                                     * else {
                                            <button className='btn btn-xs' title='Enable'
                                                onClick={() => { if (window.confirm('Active Product?')) { this.handelInactive(product.productId) } }} >
                                                <Glyphicon glyph='eye-close' />
                                            </button>
                                    }
                                 * <Glyphicon glyph='trash' /> 
                                 * className='btn btn-xs btn-danger'
                                 * <Link className='txt-primary' to={{
                                    pathName: '/ProductCreation',
                                    state: {
                                        productId: product.productId   
                                    }
                                }} > Edit </Link>*/}

                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderUsersTable(this.state.products);

        return (
            <div>
                <h1>Product List</h1>
                <p>This component demonstrates fetching data from the server.  <Link to="/ProductCreation/0" > Add User </Link> </p>
                {contents}
            </div>
        );
    }
}
