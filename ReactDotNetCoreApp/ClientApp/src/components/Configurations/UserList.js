﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class UserList extends Component {
    displayName = UserList.name

    constructor(props) {
        super(props);
        this.state = { users: [], loading: true };
        fetch('api/User/GetAllUsers')
            .then(response => response.json())
            .then(data => {
                this.setState({ users: data, loading: false });
            });
    }

    static renderUsersTable(users) {
        return (
            <table className='table'>
                <thead>
                    <tr>
                        <th>User Id</th>
                        <th>User Name</th>
                        <th>Login ID</th>
                        <th>User Role</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map(user =>
                        <tr key={user.userId}>
                            <td>{user.userId}</td>
                            <td>{user.userName}</td>
                            <td>{user.loginName}</td>
                            <td>{user.strRoleId}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : UserList.renderUsersTable(this.state.users);

        return (
            <div>
                <h1>Users</h1>
                <p>This component demonstrates fetching data from the server.  <Link to="/UserRegistration" > Add User </Link> </p> 
                {contents}
            </div>
        );
    }
}
