﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import auth from '../auth';


class productModel {
    ProductId = "";
    BarCode = ""; //Unique
    BusinessUnitId = "";
    CategoryId = "0";
    ProductName = "";
    Description = "";
    //productSpecification = "";
    Unit = "";
    OpeningQty = "0";
    SaleRate = "0";
    PurchaseRate = "0";
    Value = "0";
    Weight = "";
    MaximumQty = "0";
    MinimumQty = "0";
    Location = "";
    Status = "true";
    CreatedBy = auth.getUser();
}

export class ProductCreation extends Component {
    displayName = ProductCreation.name

    constructor(props) {
        super(props);
        const productObj = new productModel();
        productObj.BusinessUnitId = auth.getBusinessRole();
        //productObj.ProductId = "0";
        //productObj.CategoryId = "1";
        //this.state = productObj;
        this.state = { categories: [], product: productObj, message: '' };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        //debugger
        this.bindCategories();

        const { id } = this.props.match.params
        //const { productId } = this.props.location.state
        //console.log(id);

        if (id === undefined || id === '' || id === '0') {

        }
        else
            this.bindProduct(id);

        //console.log(this.state.product.CategoryId);
    }

    bindProduct(id) {
        //console.log('in');
        //debugger
        fetch(`api/Product/GetProductDetails?id=${id}`
            //fetch('api/Product/GetProductDetails/'+{id}+'',
        ).then(response => response.json())
            .then(data => {
                

                //const obbjjj = this.convertToModel(data);
                //console.log(data);

                this.setState({
                    product: this.convertToModel(data)
                });
                //console.log(this.state.product);


                //let prod = this.state.product;

                //var text = "foo bar loo zoo moo";
                //let ucfirst = data => data.toLowerCase().split(' ').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join(' ');

                //console.log(data);
                
                //console.log(this.state.product);
                //const days = data;
                //var result = days.map(d => d.replace(/(.?)/, (letter) => letter.toUpperCase()));
                //console.log(result);

                //for (let [key, value] of Object.entries(data)) {
                //    let fistword = key.charAt(0).toUpperCase();
                //    //console.log(key.replace(/(.?)/, fistword));
                //    console.log(key.replace(/(.?)/, fistword)+':', value);
                //}

                //console.log(data);

                //const toTitleCase = data => data.substr(0, 1).toUpperCase() + data.substr(1).toLowerCase();
                //console.log(toTitleCase);

                //console.log(this.titleCase(data));
                //this.titleCase(data);
                //console.log(JSON.stringify(toCamel(data)));




                //console.log(this.state.product);
                //this.setState({
                //    product: data
                //});



                //this.setState({ ...this.state.product, data });
                //console.log(this.state.product);
            });

    }

    convertToModel(data) {
        var newObj = new productModel();
        newObj.BarCode = data.barCode;
        newObj.ProductId = data.productId;;
        newObj.BusinessUnitId = data.businessUnitId;
        newObj.CategoryId = data.categoryId;
        newObj.ProductName = data.productName;
        newObj.Description = data.description;
        newObj.Unit = data.unit;
        newObj.OpeningQty = data.openingQty;
        newObj.SaleRate = data.saleRate;
        newObj.PurchaseRate = data.purchaseRate;
        newObj.Value = data.value;
        newObj.Weight = data.weight;
        newObj.MaximumQty = data.maximumQty;
        newObj.MinimumQty = data.minimumQty;
        newObj.Location = data.location;
        newObj.Status = data.status;
        newObj.CreatedBy = data.createdBy;
        return newObj;
    }

    handleChange(event) {        
        this.setState({
            product: {
                ...this.state.product,
                [event.target.name]: event.target.value
            }
        });
    }

    handdelReset() {
        console.log('Reset Called');
        //const userObj = new productModel();
        //console.log(userObj);
        //this.setState({ userObj});
    }

    bindCategories() {
        //debugger
        fetch('api/Product/GetAllCategories')
            .then(response => response.json())
            .then(data => {
                this.setState({ categories: data });
                //let teamsFromApi = data.map(team => { return { value: team, display: team } })
                //this.setState({ categories: [{ value: '', display: '(Select Category)' }].concat(teamsFromApi) });
            });
        //console.log(this.state);
    }

    handleSubmit(event) {
        //debugger;
        event.preventDefault();

        if (!event.target.checkValidity()) {
            //console.log("Error! Form Fields are requireds");
            this.setState({ message: "Error! Form Fields are requireds" });
            return;
        }
        const formValues = this.state.product;
        formValues.Status = this.refs.isActive.checked;
        //console.log(this.refs.isActive.checked);
        console.log(formValues);

        //debugger;
        fetch("api/Product/AddProduct", {
            method: "POST",
            cache: "no-cache", // *default, no-cache, reload,
            body: JSON.stringify(formValues),
            headers: {
                "Content-type": "application/json",
            }
        }).then(res => res.json())
            .then(response => {
                if (response.error === "false") {
                    //empty input fields

                }
                this.setState({ message: response.message });
                //if (response === "Success") {
                //    this.setState({ message: "Successfully Created!" });
                //    //this.props.history.push("/");
                //    //this.context.router.history.push("/dashboard");
                //    //history.push("/Dashboard");
                //} else this.setState({ message: response });
            })
            .catch(error => console.log(error));
    }

    render() {
        return (

            <div className="col-sm-12">
                <h2>Create Product</h2>
                <span className="label label-warning">{this.state.message}</span>
                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="BarCode">Bar Code : <span className="req">*</span></label>
                                <input
                                    name="BarCode"
                                    type="text"
                                    value={this.state.product.BarCode}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                    required
                                    autoFocus
                                />
                            </div>
                        </div>
                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="ProductName">Product Name : <span className="req">*</span></label>
                                <input
                                    name="ProductName"
                                    type="text"
                                    value={this.state.product.ProductName}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                    required
                                />
                            </div>
                        </div>
                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="Description">Description :</label>
                                <input
                                    name="Description"
                                    type="text"
                                    value={this.state.product.Description}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="CategoryId">Category : <span className="req">*</span></label>
                                <select className="form-control" name="CategoryId" value={this.state.product.CategoryId}
                                    onChange={e => this.handleChange(e)}>
                                    {this.state.categories.map((category) => <option key={category.categoryId} value={category.categoryId}>
                                        {category.categoryName}
                                    </option>)}
                                </select>
                            </div>
                        </div>
                        {/*
                            <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="productSpecification">Specification :</label>
                                <input
                                    name="productSpecification"
                                    type="text"
                                    value={this.state.product.productSpecification}
                                    className="form-control"
                                />
                            </div>
                        </div>
                         */}


                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="Unit">Unit :</label>
                                <input
                                    name="Unit"
                                    type="text"
                                    value={this.state.product.Unit}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                />
                            </div>
                        </div>

                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="Weight">Weight :</label>
                                <input
                                    name="Weight"
                                    type="text"
                                    value={this.state.product.Weight}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="OpeningQty">Opening Quantity :</label>
                                <input
                                    name="OpeningQty"
                                    type="number"
                                    value={this.state.product.OpeningQty}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                />
                            </div>
                        </div>
                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="PurchaseRate">Purchase Rate :</label>
                                <input
                                    name="PurchaseRate"
                                    type="number"
                                    value={this.state.product.PurchaseRate}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                />
                            </div>
                        </div>
                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="SaleRate">Sale Rate :</label>
                                <input
                                    name="SaleRate"
                                    type="number"
                                    value={this.state.product.SaleRate}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="Value">Value :</label>
                                <input
                                    name="Value"
                                    type="number"
                                    value={this.state.product.Value}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                    readOnly
                                />
                            </div>
                        </div>
                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="MaximumQty">Maximum Qty :</label>
                                <input
                                    name="MaximumQty"
                                    type="number"
                                    value={this.state.product.MaximumQty}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                />
                            </div>
                        </div>
                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="MinimumQty">Minimum Qty :</label>
                                <input
                                    name="MinimumQty"
                                    type="number"
                                    value={this.state.product.MinimumQty}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="Location">Store Location :</label>
                                <input
                                    name="Location"
                                    type="text"
                                    value={this.state.product.Location}
                                    onChange={e => this.handleChange(e)}
                                    className="form-control"
                                />
                            </div>
                        </div>

                        <div className="col-sm-4 col-xs-12">
                            <div className="form-group">
                                <label htmlFor="Status">&nbsp;</label>
                                <label className="checkbox"> &nbsp; &nbsp;
                                    <input
                                        type="checkbox"
                                        //checked={this.state.product.Status}
                                        name="Status"
                                        value={this.state.product.Status}
                                        onChange={e => this.handleChange(e)}
                                        defaultChecked
                                        ref="isActive"
                                    />

                                    Active</label>
                            </div>
                        </div>
                    </div>

                    <div className="form-group">
                        <button type="reset" className="btn btn-warning" onClick={this.handdelReset}> Reset</button> &nbsp; &nbsp;
                        <button className="btn btn-primary"> Save</button> &nbsp; &nbsp; &nbsp;
                        Go to <Link to="/ProductList"> Product List </Link>
                    </div>

                </form>

            </div>


        );
    }
}
