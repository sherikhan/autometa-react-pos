﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import auth from '../auth';

class userModel {
    UserName = "";
    LoginName = "";
    Password = "";
    strRoleId = "";
    strBusinessUnitId = "";
    AccessToken = auth.getToken();
    message = "";
}

export class UserRegistration extends Component {
    displayName = UserRegistration.name
    

    constructor(props) {
        super(props);
        const userObj = new userModel();
        //userObj.AccessToken = "1426jhkk55855TTToP233333";
        userObj.strRoleId = "1"; //Get from Login Session
        userObj.strBusinessUnitId = "1"; //Get from Login Session
        this.state = userObj;

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
        //console.log(event.target.value);
    }

    handleSubmit(event) {
        //debugger;
        event.preventDefault();

        if (!event.target.checkValidity()) {
            //console.log("Error! Form Fields are requireds");
            this.setState({ message: "Error! Form Fields are requireds" });
            return;
        }
        //const form = {
        //    UserName: this.state.username,
        //    LoginName: this.state.loginname,
        //    Password: this.state.password,
        //    strRoleId: this.state.userrole,
        //    AccessToken: this.state.AccessToken,
        //    strBusinessUnitId: this.state.BusinessUnit
        //};

        const formValues = this.state;

        debugger;
        fetch("api/User/RegisterUser", {
            method: "POST",
            cache: "no-cache", // *default, no-cache, reload,
            body: JSON.stringify(formValues),
            headers: {
                "Content-type": "application/json"
            }
        })
            .then(res => res.json())
            .then(response => {
                if (response.error === "false") {
                    //empty input fields

                }
                this.setState({ message: response.message });
                //if (response === "Success") {
                //    this.setState({ message: "Successfully Created!" });
                //    //this.props.history.push("/");
                //    //this.context.router.history.push("/dashboard");
                //    //history.push("/Dashboard");
                //} else this.setState({ message: response });
            })
            .catch(error => console.log(error));
    }

    render() {
        return (
            <div className="col-sm-5">
                <h2>Create User</h2>
                <span className="label label-warning">{this.state.message}</span>
                <form onSubmit={this.handleSubmit}>

                    <div className="form-group">
                        <label htmlFor="UserName">User Name :</label>
                        <input
                            name="UserName"
                            type="text"
                            value={this.state.UserName}
                            onChange={e => this.handChange(e)}
                            className="form-control"
                            //className={ displayError ? 'form-control displayErrors': 'form-control'}
                            required
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="LoginName">Login Id :</label>
                        <input
                            name="LoginName"
                            type="text"
                            value={this.state.LoginName}
                            onChange={e => this.handChange(e)}
                            className="form-control"
                            placeholder="like David123"
                            //className={ displayError ? 'form-control displayErrors': 'form-control'}
                            required
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="Password">Password :</label>
                        <input
                            name="Password"
                            type="password"
                            value={this.state.Password}
                            onChange={e => this.handChange(e)}
                            //className={ displayError ? 'form-control displayErrors': 'form-control'}
                            className="form-control"
                            required
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="strRoleId">User Level :</label>
                        <input
                            name="strRoleId"
                            type="text"
                            value={this.state.strRoleId}
                            className="form-control"
                            readOnly
                        />
                    </div>

                    <div className="form-group">
                        <button type="reset" className="btn btn-warning"> Reset</button> &nbsp; &nbsp;
                        <button className="btn btn-primary"> Save</button>

                        <Link to="/UserList" className="pull-right" > User List </Link> 
                    </div>

                </form>

            </div>
        );
    }
}
