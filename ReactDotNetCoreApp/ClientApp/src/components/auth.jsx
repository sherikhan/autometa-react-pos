﻿/**
 * This is a utility file you can use to store and retrieve the user token
 * if your app is using a token-based authentication strategy.
 **/

export default {

    //saveToken(token) {
    //    return localStorage.userToken = token;
    //},

    //hasToken() {
    //    return !!localStorage.userToken;
    //},

    getToken() {
        return "1426jhkk55855TTToP233333";
    },

    //deleteToken() {
    //    delete localStorage.userToken;
    //},

    //User Methods  

    saveUser(userObj) {
        localStorage.businessUnitId = userObj.strBusinessUnitId;
        localStorage.userId = userObj.userId;
        localStorage.userName = userObj.userName;
        return localStorage.userId;
    },

    hasUser() {
        return !!localStorage.userId;
    },

    getUser() {
        return localStorage.userId;
    },

    getBusinessRole() {
        return localStorage.businessUnitId;
    },

    getUserName() {
        return localStorage.userName;
    },

    distroyUser() {
        delete localStorage.businessUnitId;
        delete localStorage.userId;
        delete localStorage.userName;
    },


}