﻿import React, { Component } from 'react';
import { Glyphicon, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Link } from 'react-router-dom';
import './NavMenu.css';
import auth from './auth';

export class NavMenu extends Component {
    displayName = NavMenu.name

    IsAuthenticate() {
        return auth.hasUser();
    }

    render() {
        //debugger
        //console.log(this.IsAuthenticate());
        if (!this.IsAuthenticate())
            return null;

        return (
            <Navbar inverse fixedTop fluid collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link to={'/home'}>React & Dot Net Core </Link>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <LinkContainer to={'/home'} exact>
                            <NavItem><Glyphicon glyph='home' /> Home</NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/userRegistration'}>
                            <NavItem>
                                <Glyphicon glyph='user' /> User
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/ProductCreation/0'}>
                            <NavItem>
                                <Glyphicon glyph='th-large' /> Product
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/counter'}>
                            <NavItem>
                                <Glyphicon glyph='education' /> Counter
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/fetchdata'}>
                            <NavItem>
                                <Glyphicon glyph='th-list' /> Fetch data
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/logout'}>
                            <NavItem>
                                <Glyphicon glyph='off' /> Logout
                            </NavItem>
                        </LinkContainer>

                        <LinkContainer to={'/Common'}>
                            <NavItem>
                                <Glyphicon glyph='off' /> Common
                            </NavItem>
                        </LinkContainer>

                        <NavItem>
                            <Glyphicon glyph='user' />Welcome {auth.getUserName()}
                        </NavItem>

                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
