﻿import React, { Component } from 'react';
import auth from './auth';
import Logo from '../images/logo.jpg';

class userModel {
    UserName = "";
    LoginName = "";
    Password = "";
    strRoleId = "";
    strBusinessUnitId = "";
    AccessToken = auth.getToken();
    message = "";
}

export class Login extends Component {
    displayName = Login.name

    constructor(props) {
        super(props);
        const userObj = new userModel();
        this.state = userObj;

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        if (this.IsAuth()) {
            console.log('Already Logged in!');
            this.props.history.push("/Home");
        }
    }

    IsAuth() {
        return auth.hasUser();
    }

    handChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }


    handleSubmit(event) {
        //debugger;
        event.preventDefault();
        const formValues = this.state;
        fetch("api/User/Login", {
            method: "POST",
            cache: "no-cache", // *default, no-cache, reload,
            body: JSON.stringify(formValues),
            headers: {
                "Content-type": "application/json"
            }
        })
            .then(res => res.json())
            .then(response => {
                //console.log(response);
                if (response.error === "false") {
                    //debugger;
                    this.setState({ message: "Login Successfully!" });
                    var userObj = new userModel();
                    userObj = response.user;
                    auth.saveUser(userObj);
                    this.props.history.push("/Home");
                } else this.setState({ message: response.message });
            }).catch(error => console.log(error));
    }

    render() {
        return (
            <div className="container">
                <div className="col-sm-10 divtop">
                </div>
                <div className="col-sm-5 col-xs-12">
                    <div className='text-center Logoimg'>
                        <h3>Solution by</h3>
                        <img className="img-responsive img-centre" src={Logo} />
                    </div>
                    
                </div>
                <div className="col-sm-5 col-xs-12 loginDiv">
                    <span className="label label-warning">{this.state.message}</span>
                    <form onSubmit={this.handleSubmit}>
                        <h3>Account Login</h3>
                        <div className="form-group">
                            <label htmlFor="LoginName">Login Name :</label>
                            <input
                                name="LoginName"
                                type="text"
                                value={this.state.LoginName}
                                onChange={e => this.handChange(e)}
                                className="form-control"
                                placeholder="like David123"
                                required
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="Password">Password :</label>
                            <input
                                name="Password"
                                type="password"
                                value={this.state.Password}
                                onChange={e => this.handChange(e)}
                                className="form-control"
                                required
                            />
                        </div>

                        <div className="form-group">
                            <button className="btn btn-primary"> <span className='glyphicon glyphicon-ok'></span> Login</button>
                        </div>

                    </form>

                </div>

            </div>

        );
    }
}
