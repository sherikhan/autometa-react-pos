﻿import React, { Component } from 'react';
import auth from './auth';


export class Logout extends Component {
    displayName = Logout.name
   
    constructor(props) {
        super(props);
        this.logout();
    }

    logout() {
        //debugger
        //console.log(auth.getUserName());
        auth.distroyUser();
        this.props.history.push("/");
    }

    render() {
        return (
            <div className="col-sm-5">
                <h2>Logout</h2>
            </div>
        );
    }
}
