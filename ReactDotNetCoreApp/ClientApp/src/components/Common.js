﻿


import React, { Component, DataView } from 'react';
import { Link } from 'react-router-dom';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Glyphicon } from 'react-bootstrap';


//import ReactBsTable from 'react-bootstrap-table'
//import 'node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

export class Common extends Component {
    displayName = Common.name

    constructor(props) {
        super(props);
        this.state = { products: [], loading: true, message: '' };

        this.loadProducts();
        //console.log(this.state);
        //this.deleteClickHandler = this.deleteClickHandler.bind(this);
    }

    loadProducts() {
        fetch('api/Product/GetAllProducts')
            .then(response => response.json())
            .then(data => {
                this.setState({ products: data, loading: false });
            });
    }

    displayButtons(cell, row) {
        return (
            <DataView>
            <Link title='Edit' to={'/ProductCreation/' + row.productId}><Glyphicon glyph='edit'/></Link>
                <Link title='View' to={'/ProductCreation/' + row.productId} ><Glyphicon glyph='edit' /></Link>
            </DataView>
        );
    }

    enablebtn(productId) {
        if (window.confirm('Do you want to change status of Product?')) {
            this.handelInactive(productId)
        }
    }

    handelInactive(pram) {
        //debugger;
        fetch(`api/Product/DisableProduct/?id=${pram}`, {
            method: "POST",
            cache: "no-cache", // *default, no-cache, reload,
            headers: {
                "Content-type": "application/json"
            }
        }).then(res => res.json())
            .then(response => {
                if (response.error === 'false') {
                    //empty input fields
                    this.loadProducts();
                }
                this.setState({ message: response.message });
            })
            .catch(error => console.log(error));


    }


    renderUsersTable(products) {
        return (
            <div>
                <link rel="stylesheet" href="https://npmcdn.com/react-bootstrap-table/dist/react-bootstrap-table-all.min.css">
                </link>

                <span className="label label-warning">{this.state.message}</span>
                <BootstrapTable data={products} striped hover condensed version='4' pagination search={true}>

                    <TableHeaderColumn isKey dataField='productId'>Product Id</TableHeaderColumn>
                    <TableHeaderColumn dataField='barCode'>Bar Code</TableHeaderColumn>
                    <TableHeaderColumn dataField='productName'>Product Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='category.categoryName'>Category</TableHeaderColumn>
                    <TableHeaderColumn dataField='openingQty'>Opening Qty</TableHeaderColumn>
                    <TableHeaderColumn dataField='purchaseRate'>Purchase Rate</TableHeaderColumn>
                    <TableHeaderColumn dataField='saleRate'>Sale Rate</TableHeaderColumn>
                    <TableHeaderColumn dataField='value'>Value</TableHeaderColumn>
                    <TableHeaderColumn dataField='button' dataFormat={this.displayButtons}>Action</TableHeaderColumn>
                </BootstrapTable>

                {/* <table className='table'>
                    <thead>
                        <tr>
                            <th>Bar Code</th>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Opening Qty</th>
                            <th>Purchase Rate</th>
                            <th>Sale Rate</th>
                            <th>Value</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        {products.map(product =>
                            <tr key={product.productId}>
                                <td>{product.barCode}</td>
                                <td>{product.productName}</td>
                                <td>{product.category.categoryName}</td>
                                <td>{product.openingQty}</td>
                                <td>{product.purchaseRate}</td>
                                <td>{product.saleRate}</td>
                                <td>{product.value}</td>
                            </tr>
                        )}

                    </tbody>
                </table>*/}
            </div>
        );
    }

    render() {

        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderUsersTable(this.state.products);

        return (
            <div>
                <h1>Product List</h1>
                <p>This component demonstrates fetching data from the server.  <Link to="/ProductCreation/0" > Add User </Link> </p>
                {contents}
            </div>

        );
    }
}

