import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { UserRegistration } from './components/Configurations/UserRegistration';
import { UserList } from './components/Configurations/UserList';
import { ProductCreation } from './components/Configurations/ProductCreation';
import { Login } from './components/Login';
import { Logout } from './components/Logout';
import { ProductList } from './components/Configurations/ProductList';
import { Common } from './components/Common';


export default class App extends Component {
    displayName = App.name

    render() {
        return (
            <Layout>
                <Route exact path='/' component={Login} />
                <Route path='/home' component={Home} />
                <Route path='/userRegistration' component={UserRegistration} />
                <Route path='/counter' component={Counter} />
                <Route path='/fetchdata' component={FetchData} />
                <Route path='/UserList' component={UserList} />
                <Route path='/ProductCreation/:id' component={ProductCreation} />
                <Route path='/logout' component={Logout} />
                <Route path='/ProductList' component={ProductList} />
                <Route path='/Common' component={Common} />
            </Layout>
        );
    }
}
